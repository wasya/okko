var express = require('express');
var mongoose = require('./libs/mongoose');
var log = require('./libs/log')(module);
var session = require('express-session');

var path = require('path');
var config = require('./config/index'); // all configurations

var errorHandler = require('express-error-handler');
var HttpError = require('./errors/index').HttpError; // require  own errors handler

var favicon = require('serve-favicon'); // favicon
var morgan = require('morgan'); // logger
var bodyParser = require('body-parser'); // req.body ( for ajax, ets)
var cookieParser = require('cookie-parser'); // cookies

module.exports = function() {
    var app = express();
    
    // set view for templates and change engine
    app.set('views', path.join(__dirname + config.get('views')));
    app.set('view engine', config.get('view engine'));

    // use static firectory for public files
    app.use(express.static(__dirname + config.get('static view')));
    
    // use favicon icon for site
    app.use(favicon(__dirname + '/public/images/assets/favicon.png'));

    // set logger
    if (app.get('env') == 'development') {
        app.use(morgan('dev')); // res end
    } else {
        app.use(morgan('combined'));
    }
    
    // req.cookies
    app.use(cookieParser());
    
    app.use(session({
        secret: config.get('session:secret'),
        store: sessionStore
    }));
    
    // parse application/x-www-form-urlencoded 
    app.use(bodyParser.urlencoded({ extended: true }));
    
    // parse application/json 
    app.use(bodyParser.json());
    
    var sessionStore = require('./libs/sessionStore');

    // use errors middleware
    app.use(require('./middleware/sendHttpError'));
    
    // get all routes ( '/', '/...', ... )
    require('./routes/index')(app);
    
    // if error
    app.use(function(err, req, res, next) {
        if (typeof err == 'number') {
            err = new HttpError(err);
        } 

        if (err instanceof HttpError) {
            res.sendHttpError(err)
        } else {
            // NODE_ENV = 'production'
            if (app.get('env') == 'development') {
                //var errorHandler = errorHandler();
                //errorHandler(err, req, res, next);
            } else {
                // if production write the error log into the log
                log.error(err);
                err = new HttpError(500);
                res.sendHttpError(err);
            }
        }
    });

    return app;
}