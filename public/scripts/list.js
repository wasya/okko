$(document).ready(function() {   
    
    var updateListSelect = function() {
        sendAjaxPOST('/getList', null, disable($(this)), enable($(this)), function(data) {
            $('#listValues>option').remove();
            for (var i = 0; i < data.list.length; i++) {
                $('#listValues').append('<option value="' + data.list[i] + '">' + data.list[i] + '</option>');
            }
        });
            
    }
    
    $("body").on("click", '.add_list_button', function() {
        var listTitle = false;
        
        //listTitle = prompt('Назва нового списку: ', 'new list');
        
        $('.add_list_button').replaceWith('<a nohref"><form id="addListForm"><input type="input" placeholder="введіть назву" value=""></input><button id="confirmButton" type="button">підтвердити</button><button id="cancelButton" type="button">відміна</button></form></a>');
        
        $('#addListForm #confirmButton').click(function() {
            listTitle = $('#addListForm input').val();
            
            if (listTitle) {
                sendAjaxPOST('/createList', listTitle, disable($('.add_list_button')), enable($('.add_list_button')), function(data) {
                    $('#questionsList').append('<a nohref">' + '<span class="questions-list-title">' + listTitle + '</span>' + '<button type="button" value="' + data.listId + '" class="listIdButton"></button>' + '</a >');

                    if ($('#listValues').length > 0) {
                        updateListSelect();
                    }
                });
            } 
            
            $('#addListForm').closest('a').replaceWith('<a nohref id="listAdd" class="add_list_button"> додати новий список</a>');
        });
        
        $('#addListForm #cancelButton').click(function() {
            $('#addListForm').closest('a').replaceWith('<a nohref id="listAdd" class="add_list_button"> додати новий список</a>');
        });   
    });
        
    $("body").on("click", '.listIdButton', function() {
        var parentList = $(this).closest('a');
        var title = parentList.find('.questions-list-title').text();
        var listId = $(this).val();
        
        if ( $(this).closest('a:has(#editmenu)').length > 0 ) {
            $('#editmenu').remove();
        } else {
            $('#editmenu').remove();
            
            var buttonVal = $(this).val();
            
            $(this).after('<menu id="editmenu" type="context"></menu>');
        
            $('#editmenu').append('<menuitem id="renameMenuItem" class="list_menu_item" label="перейменувати"> перейменувати');
            $('#renameMenuItem').bind("click", function() {
                parentList.replaceWith('<a nohref"><form id="renameForm"><input type="input" placeholder="введіть нову назву" value="' + title + '"></input><button id="confirmButton" type="button">підтвердити</button><button id="cancelButton" type="button">відміна</button></form></a>');
                
                parentList = $('#renameForm');
                
                $('#renameForm #confirmButton').click(function() {
                    sendAjaxPOST('/renameList', {listId: listId, newTitle: $('#renameForm input[type="input"]').val()}, disable($(this)), enable($(this)), function() {
                        parentList.replaceWith('<span class="questions-list-title">' + $('#renameForm input[type="input"]').val() + '</span>' + 
                                               '<button type="button" value="' + listId + '" class="listIdButton"></button>');
                        updateListSelect();
                    });
                });
                
                $('#renameForm #cancelButton').click(function() {
                    parentList.replaceWith('<span class="questions-list-title">' + title + '</span>' + 
                                           '<button type="button" value="' + listId + '" class="listIdButton"></button>');
                });
            });

            $('#editmenu').append('<menuitem id="clearMenuItem" class="list_menu_item" label="очистити"> очистити');
            $('#clearMenuItem').bind("click", function() {
                sendAjaxPOST('/clearList', {listId: listId}, disable($(this)), enable($(this)), function(data) {
                    if ($('#main ul.message-fields').length > 0 && data.changed == true) {
                        $('.message_link').remove();
                        $('.field-inlist').remove();
                        $('#editmenu').remove();
                    } else {
                        $('#editmenu').remove();
                    }
                    displayMessage('Очищено');
                });
            });

            $('#editmenu').append('<menuitem id="deleteMenuItem" class="list_menu_item" label="видалити"> видалити');
            $('#deleteMenuItem').bind("click", function() {                
                sendAjaxPOST('/deleteList', {listId: listId}, disable($(this)), $('#editmenu').remove(), function(data) {
                    if (data.changed == true) {
                        window.location.reload();
                    }
                    parentList.remove();
                    updateListSelect();
                });
            });
        }
    });
    
    $('.all-questions').bind("click", function() {
        sendAjaxPOST('/allquestions', {listName: $(this).text(), inlist: false}, null, null, function() {
            window.location.href = '/questions';
        });
    });
    
    $('.new-questions').bind("click", function() {
        sendAjaxPOST('/newquestions', {listName: $(this).text(), inlist: false}, null, null, function() {
            window.location.href = '/questions';
        });
    });
    
    $('.old-questions').bind("click", function() {
        sendAjaxPOST('/oppenedquestions', {listName: $(this).text(), inlist: false}, null, null, function() {
            window.location.href = '/questions';
        });
    });
    
    $('.favorite-questions').bind("click", function() {
        sendAjaxPOST('/favoritequestions', {listName: $(this).text(), inlist: false}, null, null, function() {
            window.location.href = '/questions';
        });
    });
    
    $("body").on("click", '.questions-list-title', function() {
        sendAjaxPOST('/questions', {listName: $(this).text()}, null, null, function() {
            window.location.href = '/questions';
        });
    });
    
    return false;
});