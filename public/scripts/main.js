function sendAjaxPOST(url, updateParam, beforeSend, complete, callback) {
    $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        data: { updateParam: updateParam },
        beforeSend: beforeSend || null,
        complete: complete || null,
        statusCode: {
            200: callback,
            403: function(jqXHR) { // ERROR
                showError(jqXHR);
            }
        }
    });
}

function disable(element) {
    element.attr('disabled', 'disabled'); 
}

function enable(element) {
    element.prop('disabled', false); 
}

function showError(err) {
    var error = JSON.parse(err.responseText);

    $('body').prepend('<aside id="error-block"><p>' + error.message + '</p></aside>');
    
    setTimeout(function() {
        $('#error-block').remove();
    }, 2000);
}

function reloadPage() {
    document.location.reload(); 
}

function setText(element, text) {
    element.text(text);
}

function displayMessage(text) {
    $('body').prepend('<aside id="message-block"><p>' + text + '</p></aside>');
    
    setTimeout(function() {
        $('#message-block').remove();
    }, 2000);
}

function getPosition(e) {
  var posx = 0;
  var posy = 0;

  if (!e) var e = window.event;

  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft
      + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop
      + document.documentElement.scrollTop;
  }

  return {
    x: posx,
    y: posy
  }
}