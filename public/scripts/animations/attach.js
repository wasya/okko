$(document).ready(function() {
    $('body').on("click", '.attachBlockTrue', function() {
        $('.attachBlock').removeClass('attachBlockTrue');
        $('.attachBlock').addClass('attachBlockFalse');
        $('#questionsList').addClass('attached');
    });
    $('body').on("click", '.attachBlockFalse', function() {
        $('.attachBlock').removeClass('attachBlockFalse');
        $('.attachBlock').addClass('attachBlockTrue');
        $('#questionsList').removeClass('attached');
    });
});