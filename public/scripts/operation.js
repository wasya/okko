$(document).ready(function() {
            
    if ($('.status span').text() == 'нове') {
        sendAjaxPOST('/update', {status: 'прочитано'}, null, null, function() {
            setText($('.status span'), 'прочитано');
        });
    }
    
    var changeButton = $('#favorite');
    
    changeButton.bind("click", function() {
        if ($(this).hasClass('change_button')) {
            sendAjaxPOST('/update', {changed: true}, disable(changeButton), enable(changeButton));
            setText($('.changed span'), 'обрано');
            $('.change_button').removeClass().addClass('exchange_button');
            setText($('.exchange_button'), 'видалити з обраного');
        } else {
            sendAjaxPOST('/update', {changed: false}, disable(changeButton), enable(changeButton));
            setText($('.changed span'), 'не обрано');
            $('.exchange_button').removeClass().addClass('change_button');
            setText($('.change_button'), 'додати до обраного');
        }
    });
    
    var toListButton = $('#toList');
    
    toListButton.bind("click", function() {
        if ($('#listValues').find(':selected').text()) {
            sendAjaxPOST('/addToList', $('#listValues').find(':selected').text(), disable($(this)), enable(toListButton), function() {
                displayMessage('Додано');
            });
        }
    });
    
});