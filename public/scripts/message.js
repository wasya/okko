$(document).ready(function() {
  
  $('#messageform').submit(function() {
    var form = $(this);
    var data = form.serialize();
    $('.error-message', form).html(''); 
      
    $.ajax({
      url: '/message',
      method: 'POST',
      dataType: 'json',
      data: data,
      beforeSend: function(data) {
        form.find('input[type="submit"]').attr('disabled', 'disabled');
        $('.error-message', form).html("Надсилання...");
      },
      complete: function(data) {
        form.find('input[type="submit"]').prop('disabled', false);
      },
      statusCode: {
        200: function() {
          $('.error-message', form).html("Надіслано");
          setTimeout(function() {
            window.location.href = '/';
          }, 1000);
        },
        403: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
          $('.error-message', form).html(error.message); // show error
        }
      }
    });
    
    return false;
  });
  
});