$(document).ready(function() {
    $('.field-inlist').bind("click", function() {
        var id = $(this).val();
        
        var currentMessage = $(this).prev('a');
        var currentButton = $(this);
        
        sendAjaxPOST('/deleteFromList', {questionId: id}, disable($(this)), enable($(this)), function() {
            currentMessage.remove();
            currentButton.remove();
        });
    });
});