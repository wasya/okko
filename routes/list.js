var Message = require('../models/message').Message;
var List = require('../models/list').List;
var AuthError = require('../models/list').AuthError;
var async = require('async');
var HttpError = require('../errors/index').HttpError;
var ObjectID = require('mongodb').ObjectID;

module.exports.getList = function(req, res, next) {
    List.find({}, function(err, list) {
        if (err) return next(err);
        
        if (!list) {
            next(new AuthError('списків не знайдено'));
        }
        
        var lists = [];
        
        for (var i = 0; i < list.length; i++) {
            lists.push(list[i].title);
        }
        
        res.status(200).send({list: lists});
    });
}

module.exports.createList = function(req, res, next) {
    List.createList(req.body.updateParam, function(err, list) {
        if (err) {
            if (err instanceof AuthError) {
                return next(new HttpError(403, err.message));
            } else {
                return next(err);
            }
        }
        
        res.status(200).send({listId: list._id});
    });
}

module.exports.renameList = function(req, res, next) { // перевірити чи нова назва вільна!!!!!!!!!!
    List.findOne({title: req.body.updateParam.newTitle}, function(err, data) {
        if (err) return next(err);
        
        if (data) return next(new HttpError(403, "список вже існує"));
        
        List.findByIdAndUpdate(req.body.updateParam.listId, {title: req.body.updateParam.newTitle}, function(err, list) {
            if (err) return next(err);

            if (!list) return next(new HttpError(403, 'список не існує'));

            res.sendStatus(200);
        });
    });
}

module.exports.clearList = function(req, res, next) {
    console.log(req.body.updateParam.listId);
    List.findByIdAndUpdate(req.body.updateParam.listId, {questions: []}, function(err, list) {
        if (err) return next(err);
        
        if (!list) {
            return next(new HttpError(403, 'список не знайдено'))
        };
        
        var change = req.body.updateParam.listId == req.session.lastList ? true : false;
        
        res.status(200).send({changed: change});
    });
}

module.exports.deleteList = function(req, res, next) {
    List.remove({_id: req.body.updateParam.listId}, function(err) {
        if (err) {
            if (err instanceof AuthError) {
                return next(new HttpError(403, "Помилка видалення"));
            } else {
                return next(err);
            }
        }

        var change = req.body.updateParam.listId == req.session.lastList ? true : false;
        
        if (change) {
            req.session.lastList = undefined;
            req.session.searchParams = {};
        }
        
        res.status(200).send({changed: change});
    });
}