var Message = require('../models/message').Message;
var AuthError = require('../models/message').AuthError;
var async = require('async');
var HttpError = require('../errors/index').HttpError;

module.exports.get = function(req, res) {
    res.render('pages/message');
}

module.exports.post = function(req, res, next) {
    var messageParams = {};
    
    messageParams.name = req.body.fio; // fio
    messageParams.card = req.body.card; // card
    messageParams.mail = req.body.mail; // mail
    messageParams.phone = req.body.phone; // phone
    messageParams.subject = req.body.subject; // subject
    messageParams.messageText = req.body.text; // message
        
    Message.registrate(messageParams, function(err, message) {
        if (err) {
            if (err instanceof AuthError) {
                return next(new HttpError(403, err.message));
            } else {
                return next(err);
            }
        }
        
        req.session.message = message._id;
        res.sendStatus(200);
    });
}