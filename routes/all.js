var HttpError = require('../errors/index').HttpError;

module.exports.all = function(req, res, next) {
    next(new HttpError(404, "Page is not found"));
}