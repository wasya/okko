var Message = require('../models/message').Message;
var List = require('../models/list').List;
var async = require('async');
var HttpError = require('../errors/index').HttpError;

module.exports.get = function(req, res, next) {    
    Message.find(req.session.searchParams, function(err, messages) {
        if (err) return next(err);
        
        List.find({}, function(err, list) {
            if (err) return next(err);
            
            console.log('current list: ' + req.session.lastList);
            
            res.render('pages/questions', {
                questions: messages,
                lists: list,
                inlist: req.session.lastList
            });
        });
    });
}

module.exports.post = function(req, res, next) { // check all errors !!!!!!!!!!!!!! don`t forget
    
    if (req.body.updateParam.inlist) {
        req.session.lastList = req.session.lastList = undefined;
        console.log(req.body.updateParam.inlist);
    }
    
    async.waterfall([
        function(callback) {
            List.findOne({title: req.body.updateParam.listName}, function(err, lists) {
                if (err) return next(err);

                if (!lists) {
                    console.log('empty');
                    req.session.searchParams = {};
                    req.session.lastList = undefined;
                    res.status(200).send();
                    return;
                }
                
                req.session.lastList = lists._id;
                callback(null, lists.questions);
            });  
        },
        function(listQuestions, callback) {
            var questions = [];
                        
            for (var i = 0; i < listQuestions.length; i++) {
                questions.push(listQuestions[i].id);
            }
            
            req.session.searchParams = {_id: {$in: questions}};

            callback(null, questions);
        }
    ], function(err, messages) {
        if (err) return next(err);

        res.status(200).send();
    });

}

module.exports.all = function(req, res, next) { // check all errors !!!!!!!!!!!!!! don`t forget
    req.session.searchParams = {};
    req.session.lastList = undefined;
    res.sendStatus(200);
}

module.exports.new = function(req, res, next) { // check all errors !!!!!!!!!!!!!! don`t forget
    req.session.searchParams = {status: 'нове'};
    req.session.lastList = undefined;
    res.sendStatus(200);
}

module.exports.oppened = function(req, res, next) { // check all errors !!!!!!!!!!!!!! don`t forget
    req.session.searchParams = {status: 'прочитано'};
    req.session.lastList = undefined;
    res.sendStatus(200);
}

module.exports.favorite = function(req, res, next) { // check all errors !!!!!!!!!!!!!! don`t forget
    req.session.searchParams = {changed: true};
    req.session.lastList = undefined;
    res.sendStatus(200);
}