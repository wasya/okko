var Message = require('../models/message').Message;
var List = require('../models/list').List;
var AuthError = require('../models/list').AuthError;
var async = require('async');
var HttpError = require('../errors/index').HttpError;
var ObjectID = require('mongodb').ObjectID;

module.exports.get = function(req, res, next) {
    
    req.session.lastQuestion = req.param('id');
        
    try {
            var id = new ObjectID(req.params.id);
        } catch (e) {
            return next(new HttpError(404, "Message not found"));
        }
        
    Message.findById(req.params.id, function(err, question) {

        if (err) return next(err);

        if (!question) next(404);
                
        List.find({}, function(err, list) {
            if (err) return next(err);
            
            res.render('pages/question', {
                message: question,
                favorite: question.changed == true ? 'exchange' : 'change',
                lists: list
            });
        }); 
    });
}

module.exports.updateParams = function(req, res, next) {
    
    Message.findByIdAndUpdate(req.session.lastQuestion, req.body.updateParam, {upsert:true}, function(err, result) {
        if (err) return next(err);
        
        res.sendStatus(200);
    });
    
}

module.exports.deleteFromList = function(req, res, next) {
    async.waterfall([
        function(callback) {
            List.findById(req.session.lastList, function(err, list) {
                if (err) return next(err);
                
                if (!list) {
                    callback(null, null);
                }
                
                callback(null, list.questions);
            });
        }, 
        function(questions, callback) {
            
            var indexToRemove = questions.findIndex(obj => obj.id == req.body.updateParam.questionId);
            questions.splice(indexToRemove , 1);
            
            callback(null, questions);
        },
        function(formatedQuestions, callback) {
            List.findByIdAndUpdate(req.session.lastList, {questions: formatedQuestions}, function(err, list) {
                if (err) return next(err);
                
                callback(null, list);
            });
        }
    ], function(err, data) {
        if (err) return next(err);
        
        res.status(200).send();
    });
}

module.exports.addToList = function(req, res, next) { 
    async.waterfall([
        function(callback) {
            List.findOne({title: req.body.updateParam}, function(err, list) {
                if (err) return next(err);
                
                if (!list) {
                    callback(null, null);
                }
                
                Message.findById(new ObjectID(req.session.lastQuestion), function(err, message) {
                    if (err) return next(err);
                    
                    callback(null, list, message);
                });
            });
        }, 
        function(list, message, callback) {
            List.findOne({title: req.body.updateParam}).where("questions").elemMatch({id: message._id}).exec(function(err, data) {
                if (err) return next(err);
                
                if (data) {
                    return next(new HttpError(403, "Повідомлення вже в списку"));
                }
                
                callback(null, list, message);
            });
        },
        function(list, message, callback) { 
            
            
            
            List.findByIdAndUpdate(list._id, {$push: {"questions": {id: message._id, subject: message.subject, date: message.date, mail: message.mail}}}, {safe: true, upsert: true, new : true}, function(err, data) {
                if (err) callback(err, null);
                
                callback(null, data);
            });
        }
    ], function(err, listQuestion) {
        if (err) return next(err);
        
        res.sendStatus(200);
    });
}