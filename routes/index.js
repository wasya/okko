module.exports = function(app) {
    
    app.get('/', require('./frontpage').get);
    
    app.get('/message', require('./message').get);
    app.post('/message', require('./message').post);
    
    app.get('/questions', require('./questions').get);
    app.post('/questions', require('./questions').post);
    app.post('/allquestions', require('./questions').all);
    app.post('/newquestions', require('./questions').new);
    app.post('/oppenedquestions', require('./questions').oppened);
    app.post('/favoritequestions', require('./questions').favorite);
    
    app.post('/getList', require('./list').getList);
    app.post('/createList', require('./list').createList);
    app.post('/renameList', require('./list').renameList);
    app.post('/clearList', require('./list').clearList);
    app.post('/deleteList', require('./list').deleteList);
    
    app.get('/question/:id', require('./question').get);
    app.post('/update', require('./question').updateParams);
    app.post('/deleteFromList', require('./question').deleteFromList);
    app.post('/addToList', require('./question').addToList);
    
    app.all('/*', require('./all').all);

}