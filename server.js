var http = require('http');
var path = require('path');
var config = require('./config/index'); // get configurations

var express = require('./express'),
    app = express();

// create server
var server = http.createServer(app);

server.listen(config.get('port'), function() { // listen port
    console.log("Server work on port " + config.get('port') + "..."); // All work
});