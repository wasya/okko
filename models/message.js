var async = require('async');
var util = require('util');

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
  
  name: {
    type: String,
    required: true
  },
  card: {
    type: String,
    required: true
  },
  mail: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  subject: {
    type: String,
    required: true
  },
  messageText: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  changed: {
    type: Boolean,
    default: false
  },
  status: {
    type: String,
    default: 'нове'
  }
  
});

schema.statics.registrate = function(params, callback) {
    var Message = this;
    
    async.waterfall([
        function(callback) {
            Message.findOne({subject: params.subject, mail: params.mail}, callback);
        },
        function(message, callback) { // save message in base
            if (message) {
                callback(new AuthError("Ваша тема з вже існує"));
            } else {
                var message = new Message({
                    name: params.name || '',
                    card: params.card || '',
                    mail: params.mail || '',
                    phone: params.phone || '',
                    subject: params.subject || '',
                    messageText: params.messageText || ''
                }); 

                    message.save(function(err) {
                    if (err) return callback(err);
                    callback(null, message);
                });
            }
            
        }
    ], callback);
}

var MessageModel = mongoose.model('Message', schema);

module.exports.Message = MessageModel;

function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);
    
    this.message = message;
}

util.inherits(AuthError, Error);

AuthError.prototype.name = 'AuthError';

module.exports.AuthError = AuthError;