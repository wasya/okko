var async = require('async');
var util = require('util');

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
  
  title: {
    type: String,
    required: true
  },
  questions: [{
      id: Schema.Types.ObjectId, 
      mail: String,
      date: Date,
      subject: String
  }]
  
});

schema.statics.createList = function(title, callback) {
    var List = this;
    
    async.waterfall([
        function(callback) {
            List.findOne({title: title}, callback);
        },
        function(list, callback) { // save message in base
            if (list) {
                callback(new AuthError("Список вже існує"));
            } else {
                var list = new List({
                    title: title,
                    questions: []
                }); 

                list.save(function(err) {
                    if (err) return callback(err);
                    callback(null, list);
                });
            }
            
        }
    ], callback);
}

var ListModel = mongoose.model('List', schema);

module.exports.List = ListModel;

function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);
    
    this.message = message;
}

util.inherits(AuthError, Error);

AuthError.prototype.name = 'AuthError';

module.exports.AuthError = AuthError;